cd $(dirname $(realpath $0))
mkdir -p data/pdb-bcif data/pdb-cif data/alphafold-cif

for ID in $(cat list-pdb.txt); do wget -O "data/pdb-bcif/$ID.bcif" "https://www.ebi.ac.uk/pdbe/entry-files/download/${ID}.bcif"; done
for ID in $(cat list-pdb.txt); do wget -O "data/pdb-cif/$ID.cif" "https://www.ebi.ac.uk/pdbe/entry-files/download/${ID}_updated.cif"; done
for ID in $(cat list-alphafold.txt); do wget -O "data/alphafold-cif/AF-$ID.cif" "https://alphafold.ebi.ac.uk/files/AF-${ID}-F1-model_v4.cif"; done
