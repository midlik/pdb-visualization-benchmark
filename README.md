# pdb-visualization-benchmark

Benchmarking dataset of PDB structures (various molecule types, experimental methods, weird cases etc.). 
This set was created for testing visualization, but could be useful for other applications.

There is also a nice collection of interesting entries here: https://proteopedia.org/wiki/index.php/Believe_It_or_Not

## List of selected PDB entries

- Various molecule types
  - Protein:
    - `1wo0` - small monomer (18res), NMR, currently shown with sticks
    - `8b9r` - small monomer (40res), NMR, currently shown with cartoon
    - `1cbs` - medium monomer (140res)
    - `1hda` - medium tetramer (140res), multiple copies of 1 entity in the assembly (hemoglobin)
    - `6um1` - large monomer (2500res) 
    - `6u5u` - large heterodimer (2000res)
    - `5nik` - just nice, prolate shape
    - `7q0a` - just nice, COVID spike protein
    
  - DNA: 
    - `8ask` - small (12res)
    - `8ash` - small (14res) with ligand, 
    - `7xv8` - small (18res) with protein (77res)
    - `7euc` - small quadruplex, chains of different lengths (4, 5, 6res)
    - `1ybl` - small i-motif (6res)
    - `8atf` - medium (220res), with large protein (1100res)
    - `6hkt` - large (1100res), on histones
    - `7arv`, `7are`, `7as5` - large (8000res) + many oligos, EM, (DNA origami)

  - RNA:
    - `8amj` - small (9res)
    - `1a1t` - small (20res) with small protein (55res)
    - `8e3i` - small (11res) with large protein
    - `6mec` - medium RNA (860res) with protein and small DNA, EM
    - `8d8k` - many RNA chains, up to 1700res (ribosome subunit)
    - `8eiu` - many RNA chains, up to 2900res (ribosome)
    - `7am2` - heaviest molecule so far (Feb2023), 19000res RNA, EM (ribosome)

  - DNA/RNA hybrid:
    - `100d` - small (10res) with ligand
    - `1d9d` - small (6res) with protein
    - `8aag` - medium (200res) on histone (8aag), current image generation has wrong zoom

  - Sugar:
    - `6q4r` - linked (2res, 1res), many ligands
    - `3d11` - linked, branched (5res, 2res), 1res
    - `6ezn`, `6s7t` - linked, branched (8res), 2D sugar images are cropped
    - `5t3x` - lots of glycosylation, nicely branched sugars
    - `5elb` - free, branched (4res), alternative locations
    - `2eqd` - free, linear (8res)
    - `1tb6` - free, linear (16res)
    - `2bvk` - free, linear (8res), only sugar
    - `3irl` - free, linear (36res), only sugar, 2D sugar image missing
    - `1c58` - free, cyclic (26res), only sugar

  - PNA (peptide nucleic acid):
    - `3c1p` - small tetramer (8res), current image generation is weird (interacting sidechains not visible)
    - `176d` - small PNA:RNA hybrid (6res), NMR
    - `5eme` - small PNA:RNA hybrid (8res), X-ray

  - ANA (altritol nucleic acid, _entity_poly.type=other):
    - `3ok2`, `3ok4` - small ANA:RNA hybrid (10res)

  - Polypeptide(D):
    - `6ann` - small (4res) with ligands, current image generation is weird (cartoon only)
    - `7ajz` - small (4res) with protein
    - `6bes` - small cyclic (11res)

  - Cyclic pseudo-peptide (there is a dedicated _entity_poly.type for this, but it's not used):
    - `3t4g` - small cyclic (14res), current image generation is weird (cartoon only, cycle not visible)
    - `3q9j` - small cyclic (10res)
    - `6ugc` - small cyclic (9res) with Na and Cd ions

  - Antibiotic peptide:
    - `1fvm` - 7res with cycles (vancomycin), 3res
    - `1qd8` - 7res with cycles (vancomycin), alternative locations
    - `7nre` - 7res with cycles (darobactin)
    - `8adi` - 7res with cycles (darobactin), EM
    - `6djk` - 11res

  - Lipid:
    - `6yfy`
    - `3k2s` - phospholipid in many copies around a protein
  
  - Metal cluster:
    - `1a70` - bound Fe2S2 (Cys-Fe bond)
    - `7e5z` - covalently bound Fe2S2, Fe4S4, W (Cys-S bond)
    - `4dhv` - Fe3AgS4, Co(NH3)6
    - `5fle` - CFe3NNiS4
    - `4jc0` - FS5
    - `6sg2` - LFH
    - `8aio` - MXF
    - `6h8h` - GX2 (H17Mo14O47)
    - `7z5j` - IHW tungstate (W16O58), EM
    - `6hyb` - ZRW (H2O61P2W17Zr)
    - `6qsh` - JGH keggin (CeW22H2P2O78), alternative locations

  - Ion: 
    - `5ufq` - Mg, Ca, Cd, Cl
    - `6ufo` - K, Zn, PO4
    - `6ugc` - Na, Cd
    - `1qd8` - Cl
    - `3d11` - I
    - `7bey` - SO4
  
  - Bound cofactor:
    - `1bvy`, `1hda` - heme
    - `1og2` - heme interacting with water

  - Combinations:
    - `8hra` - large protein (950res) with small RNA (20res), NMR, current image generation fails
    - `8hrb` - large protein (950res) with small RNA (20res), X-ray, current image generation fails
    - `8h0v` - protein + DNA + RNA, EM (RNA polymerase)
    - `8ehq` - protein + DNA + RNA, EM (transcription complex), current image generation fails
    - `5it7` - protein + DNA + RNA, EM (ribosome with virus)
    - `6l4a` - protein + DNA, just nice (nucleosomes)


- Assembly combinations:
  - `1hda` - entry heterotetramer = assembly (hemoglobin)
  - `1tqn` - entry monomer, assemblies mono and tetra (CYP)
  - `1og2` - entry dimer, assemblies monomers (CYP)
  - `1a0q` - entry dimer = assembly (immunoglobulin)
  - `12e8` - entry tetramer, assemblies dimers (immunoglobulin)
  
- Large symmetric assemblies
  - Spherical
    - `6vfi` - protein heterodimer, assembly = 24x 37 kDa (designed nanoparticle)
    - `1smv` - protein trimer, assembly = 60x 86 kDa (virus)
    - `1a34` - protein + RNA, assembly = 60x 24 kDa (virus)
    - `1aq3` - protein + RNA, assembly = 60x 53 kDa (virus)
    - `4v7q` - protein, assembly = 60x 1.4 MDa (rotavirus)
    - `5j7v` - protein trimer, assembly = 2760x 215 kDa = 594 MDa, heaviest assembly so far (Feb2023) (virus)
  - Repetitive/helical
    - `4v3p` - supra-molecular helix of ribosomes
    - `7pqc` - repetitive 14-mer = assembly, EM (tubulin)
    - `3kml` - circular 34-mer = assembly (tobacco mosaic virus)
    - `2tmv` - protein monomer + RNA, helical assembly = 49x 19 kDa (tobacco mosaic virus), fiber diffraction
  - Phages
    - `5vf3`, `7vs5` - phage head
    - `5iv5` - phage baseplate

- Huge structures
  - `3j3q` - 34 MDa entry, 1 entity, EM (HIV capsid)
  - `7y5e` - 18 MDa entry, EM
  - `7y7a` - heaviest entry so far (Feb2023), 36 MDa, many entities, EM (photosyntesis complex)
  - `7am2` - heaviest molecule so far (Feb2023), 19000res RNA, EM (ribosome)
  - `5j7v` - heaviest assembly so far (Feb2023), assembly = 2760x 215 kDa = 594 MDa, EM (virus)

- Modified residues
  - On protein:
    - `1gkt` - SUI
    - `1l7c` - MSE, multiple assemblies
    - `1hcj` - more types of modified residues GYS, ABA
    - `2n4n` - designed peptide (25res) with 4G6, 4FU, current API misses modified residues, current image generation shows no linkage on modified residues
  - On DNA:
    - `1d9d` - U31, C31

- Chain numberings label vs auth
  - `1bvy` (label A,B,C = auth A,F,B)
  - `12e8` (label A,B,C,D = auth L,H,M,P)
  - `5w97` (label A,N,B,O,C,P... = auth A,a,B,b,C,c...)
- Residue numbering label vs auth
  - `2k7q` - negative auth numbers and large gaps

- SIFTS:
  - `5cim`, `2k7q` - interleaving CATH domains
  - `2ww8`, `1n26` - multiple instances of the same family in one chain, interleaving
  - `1hda` - multiple instances of the same family in different chains in assembly, current image generation gives wrong captions
  - `1og2`, `1bvy` - multiple instances of the same family in different chains in entry
  - `2gcv` - Rfam mapping, MES ligand incomplete

- Trace-only models:
  - `2rcj` - trace-only
  - `1jgq` - part trace-only, part all-atom

- Missing residues: 
 - `1tqn`
 - `6ezn`


- Experimental methods:
  - X-ray - almost everything
  - `176d`, `1wo0`, `8b9r` - solution NMR, small size
  - `1wrf`, `1jv8`, `1x4m` - solution NMR, medium size
  - `2kox` - solution NMR, large number of models (640)
  - `1q7o`, `1nh4`, `2e8d` - solid-state NMR
  - `1io5`, `1wq2` - neutron diffraction
  - `7arv`, `7are`, `7as5`, `6mec`, `8adi`, `7z5j`, `8h0v`, `8ehq`, `5it7`, `7am2`, `7y7a`, `3j3q` - EM
  - `1eg0` - structures fit into EM map
  - `1at9`, `3b5u`, `2dfs` - electron crystallography
  - `1f6h` - X-ray powder diffraction
  - `2tmv`, `1aga` - fiber diffraction
  - `1haq`, `3k2s` - solution scattering
  - `1rmn` - fluorescence transfer
  - `1ssz` - infrared spectroscopy (multi-model)
  - `7yk9` - hybrid (X-ray + neutron diffraction)
  - `7bxx` - hybrid (X-ray + solution scattering)
  - `2kkw` - hybrid (NMR + EPR)
  - `1zyt` - hybrid (X-ray + EPR)
  - `2bvk` - hybrid (solution NRM + theretical model)


- AlphaFold DB
  - `Q16743` - human CYP 2C, good quality
  - `Q76EI6` - rat fatty acid receptor, low-pLDDT tail
  - `Q8W3K0`, `A0A6P6WSC7` - probable disease resistance protein
  - `Q5VSL9` - poor loops
  - `P71291`, `A2NUJ7` - very poor prediction

- Otherwise interesting entries:
  - Assemblies with a lot of free space (models fit into EM map)
    - `2agn` - RNA
    - `5zz8`, `5tsk` - viruses
  - `5tdk` - unusual DNA linkages
  
